#include <stdio.h>
int fibonacciSeq(int n)
{
	if (n==0||n==1) return n;
	else return fibonacciSeq(n-1)+fibonacciSeq(n-2);
}
int main()
{
	int x,n;
	printf("Enter a number: ");
	scanf("%d",&x);
	for (n=0;n<x;n++)
	{
		printf("%d\n",fibonacciSeq(n));
	}
	return 0;
}
